import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./stepform.css";
import $ from "jquery";
import { Form } from "react-bootstrap";
import ProgressBar from "react-bootstrap/ProgressBar";
import { withRouter } from "react-router-dom";
import axios from "axios";
import config from "./config.json";
import { injectIntl } from "react-intl";
import { FormattedMessage } from "react-intl";
import Popup from '../Dialog/dialog';

const mainurl = Object.values(config["URL"]);
const URL = mainurl.join("");
const mainURL = URL;

const now = 40;
class Step2 extends React.Component {
  constructor(props, context) {
    super(props);
    this.state = {
      tutoring: "",
      remunation: "",
      legalsection: "",
      selected: [],
      remunerationstatus: false,
      remunerationagree: false,
      tutoringstatus: false,
      tutoringagree: false,
      start: false,
      all_headings: [],
      heading_arr: JSON.parse(localStorage.getItem("heading_arr")),
      seen: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    console.info("strt", this.props.status);
    JSON.parse(localStorage.getItem("selectedStep2"))
      ? this.setState({
          selected: JSON.parse(localStorage.getItem("selectedStep2")),
        })
      : this.setState({});
    axios
      .get(mainURL + "/step2", {
        params: {
          user: localStorage.getItem("user"),
          lang: localStorage.getItem("langs") ? localStorage.getItem("langs") : 'en',
        },
      })
      .then((res) => {
        console.info(res.data);
        this.setState({
          all_headings: res.data,
          // selected: res.data.answers
        });
      });
    localStorage.setItem("all_heading", this.state.all_headings.length);
    console.info("answer list", this.state.selected);
  }

  remuneration(ans) {
    //     console.info(ans);
    this.setState({
      remunation: ans,
    });
    if (ans == "agree") {
      document.getElementById("remunation").style.display = "block";
    } else {
      document.getElementById("remunation").style.display = "none";
    }
  }
  handleSubmit(event) {
    // alert("A name was submitted: " + this.state.value);
    event.preventDefault();
    // this.props.history.push(`/test3`);
    let axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
      },
    };
    axios({
      method: "post",
      url: mainURL + "/step2/post/steptwo",
      data: {
        steponeans: this.state.selected,
        userid: localStorage.getItem("user"),
      },

      header: axiosConfig,
    }).then((response) => {
      if (response.status == "success") {
        this.props.history.push(`/test3`);
      }
    });
    this.props.history.push(`/test3`);
  }

  tutoring(ans) {
    //     console.info(ans);
    this.setState({
      tutoring: ans,
    });
    if (ans == "agree") {
      document.getElementById("legaldiv").style.display = "block";
    } else {
      document.getElementById("legaldiv").style.display = "none";
    }
  }
  gotoPrvStp() {
    this.props.history.push(`/test`);
  }


  legalbox(id, ans, question, heading_id, heading) {
    const { intl } = this.props;
    console.info("id", id);
    console.info("answor", ans);
    console.info("anssswor", question);
    console.info(
      "after change anssswor",
      intl.formatMessage({
        id: "douoff",
        defaultMessage: "Do you offer a financial remuneration to interns?",
      })
    );
    if (
      question ===
        intl.formatMessage({
          id: "douoff",
          defaultMessage:
            "Do you offer a financial remuneration to interns?",
        }) &&
      ans === intl.formatMessage({ id: "agree", defaultMessage: "Agree" })
    ) {

      this.setState({
        remunerationstatus: true,
        remunerationagree: true,
      });
      console.info("ender if", this.state.remunerationstatus);
    }
    if (
      question ===
        intl.formatMessage({
          id: "douoff",
          defaultMessage:
            "Do you offer a financial remuneration to interns?",
        }) &&
      (ans ===
        intl.formatMessage({ id: "disagree", defaultMessage: "Disagree" }) ||
        ans ===
          intl.formatMessage({ id: "idontknow", defaultMessage: "Don't know" }))
    ) {
      this.setState({
        remunerationstatus: false,
        remunerationagree: false,
      });
    }
    if (
      question ===
        intl.formatMessage({
          id: "douat",
          defaultMessage: "Do you assign a tutor for each intern?",
        }) &&
      ans === intl.formatMessage({ id: "agree", defaultMessage: "Agree" })
    ) {
      this.setState({
        tutoringstatus: true,
        tutoringsagree: true,
      });
      console.info("ender if", this.state.tutoringstatus);
    }
    if (
      question ===
        intl.formatMessage({
          id: "douat",
          defaultMessage: "Do you assign a tutor for each intern?",
        }) &&
      (ans ===
        intl.formatMessage({ id: "disagree", defaultMessage: "Disagree" }) ||
        ans ===
          intl.formatMessage({ id: "idontknow", defaultMessage: "Don't know" }))
    ) {
      this.setState({
        tutoringstatus: false,
        tutoringsagree: false,
      });
      console.info("ender if", this.state.tutoringstatus);
    }
// for popup and radio button inside that
    if (
      question ===
        intl.formatMessage({
          id: "douensure",
          defaultMessage: "Do you ensure a reasonable ratio of interns per tutor?",
        }) &&
      ans === intl.formatMessage({ id: "agree", defaultMessage: "Agree" })
    ) {
      this.setState({
        seen: true
      });
      console.info("popup seen", this.state.seen);
    }
    if (
      question ===
        intl.formatMessage({
          id: "douensure",
          defaultMessage: "Do you ensure a reasonable ratio of interns per tutor?",
        }) &&
      (ans ===
        intl.formatMessage({ id: "disagree", defaultMessage: "Disagree" }) ||
        ans ===
          intl.formatMessage({ id: "idontknow", defaultMessage: "Don't know" }))
    ) {
      this.setState({
        seen: false
      });
      console.info("popup not seen", this.state.seen);
    }

    let tasks = this.state.selected;
    if (tasks.filter((item) => item.id == id).length == 0) {
      tasks.push({
        id: id,
        ans: ans,
        heading_id: heading_id,
        heading: heading,
        question: question,
      });
    } else {
      tasks.forEach((element, index) => {
        if (element.id === id) {
          tasks[index]["id"] = id;
          tasks[index]["ans"] = ans;
          tasks[index]["heading_id"] = heading_id;
          tasks[index]["heading"] = heading;
          tasks[index]["question"] = question;
        }
      });
    }
    this.setState({
      selected: tasks,
    });
    console.log("step 2", this.state.selected);
  }
  handleYesStep2 = () => {
    const { intl } = this.props;
    //for total 'yes' calculation
    let i = 0;
    this.state.selected.map((renumeration) =>
      renumeration.ans ==
      intl.formatMessage({ id: "agree", defaultMessage: "Agree" })
        ? i++
        : ""
    );
    console.log("total yes in step 2", i);
    this.props.handleYesAndResponse2(i, this.state.selected.length);
    localStorage.setItem("selectedStep2", JSON.stringify(this.state.selected));
    // for section wise result calculation
    for (let i = 3; i < 3 + this.state.all_headings.length; i++) {
      let arr = this.state.heading_arr;
      let selected_arr = this.state.selected;
      const count = selected_arr.reduce(
        (acc, cur) =>
          cur.heading_id === i &&
          cur.ans ===
            intl.formatMessage({ id: "agree", defaultMessage: "Agree" })
            ? ++acc
            : acc,
        0
      );
      const total_count = selected_arr.reduce(
        (a, c) => (c.heading_id === i ? ++a : a),
        0
      );

      if (arr.filter((item) => item.heading_id == i).length == 0) {
        console.log("index is", arr);
        arr.push({
          heading_id: i,
          count: count,
          arr_length: total_count,
          heading: this.state.all_headings[i - 3].heading,
        });
      } else {
        arr.forEach((element, index) => {
          if (element.heading_id === i) {
            arr[i - 1]["heading_id"] = i;
            arr[i - 1]["count"] = count;
            arr[i - 1]["arr_length"] = total_count;
            arr[i - 1]["heading"] = this.state.all_headings[i - 3].heading;
          }
        });
      }
      this.setState({
        heading_arr: arr,
      });
      console.log("heading array for step2 is:", this.state.heading_arr);
    }
    localStorage.setItem("heading_arr", JSON.stringify(this.state.heading_arr));

    // for concating one object array to other one
    let selected_array = JSON.parse(localStorage.getItem("selected"));
    var hash = {};
    (selected_array).forEach(function (a) {
      hash[a.question] = true;
    });
    (this.state.selected).forEach(function (a) {
        hash[a.question] || (selected_array).push(a);
    });
    
    console.log("selected array", selected_array);
    localStorage.setItem("selected1", JSON.stringify(selected_array));
  };

  render() {
    const { intl } = this.props;
    const { all_questions, all_headings } = this.state;

    return (
      <React.Fragment>
        <section className="progress-sec">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <ol className="progtrckr">
                  <li className="progtrckr-done" value="0">
                    <em>1</em>
                    <span>
                      <FormattedMessage
                        id="step1_1"
                        defaultMessage="Recruitment &amp; Written agreement"
                      />
                    </span>
                  </li>
                  <li className="progtrckr-doing" value="1">
                    <em>2</em>
                    <span>
                      <FormattedMessage
                        id="step1_2"
                        defaultMessage="Remuneration, Skills, Tutoring"
                      />
                    </span>
                  </li>
                  <li className="progtrckr-todo" value="2">
                    <em>3</em>
                    <span>
                      <FormattedMessage
                        id="step1_3"
                        defaultMessage="Career, Work Environment, Social Protection"
                      />
                    </span>
                  </li>
                </ol>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 mb-30">
                <ProgressBar now={now} label={`${now}%`} />
              </div>
            </div>
          </div>
        </section>
        <section className="question-sec">
          <div className="container">
            <Form onSubmit={this.handleSubmit}>
              {all_headings.map((item, i) => (
                <React.Fragment>
                  <div className="row">
                    <div className="col-md-12">
                      <h1 className="main-heading ques-heading">
                        {item.heading}
                      </h1>
                      <div className="hr-line"></div>
                    </div>
                  </div>
                  {item.questions.map((questionitem, i) =>
                    questionitem.status == 1 ? (
                      <div className="row">
                        <div className="col-md-7">
                          <p className="survey-question">
                            {questionitem.question}
                          </p>
                        </div>
                        <div className="col-md-5" key={i}>
                          <div className="answer-inputs">
                            <div className="ans-status">
                              <p>
                                {this.state.selected.find(
                                  (a) => a["id"] === questionitem.id
                                )
                                  ? this.state.selected.find(
                                      (a) => a["id"] === questionitem.id
                                    )["ans"]
                                  : questionitem.user_id == item.user &&
                                    questionitem.answer}
                              </p>
                            </div>
                            <div className="ans-selection like">
                              <input
                                title={intl.formatMessage({
                                  id: "agree",
                                  defaultMessage: "Agree",
                                })}
                                type="radio"
                                id="rec-q-1-like"
                                name={questionitem.id}
                                value="agree"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "agree",
                                      defaultMessage: "Agree",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == 
                                  intl.formatMessage({
                                    id: "agree",
                                    defaultMessage: "Agree",
                                  }) &&
                                  questionitem.user_id == item.user
                                    ? "checked"
                                    : ""
                                }
                                required
                              />
                              <i className="far fa-thumbs-up"></i>
                            </div>
                            <div className="ans-selection dislike">
                              <input
                                title={intl.formatMessage({
                                  id: "disagree",
                                  defaultMessage: "Disagree",
                                })}
                                type="radio"
                                id="rec-q-1-dislike"
                                name={questionitem.id}
                                value="disagree"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "disagree",
                                      defaultMessage: "Disagree",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == 
                                  intl.formatMessage({
                                    id: "disagree",
                                    defaultMessage: "Disagree",
                                  }) &&
                                  questionitem.user_id == item.user
                                    ? "checked"
                                    : ""
                                }
                                required
                              />
                              <i className="far fa-thumbs-down"></i>
                            </div>
                            <div className="ans-selection dnknow">
                              <input
                                title={intl.formatMessage({
                                  id: "idontknow",
                                  defaultMessage: "Don't know",
                                })}
                                type="radio"
                                id="other"
                                name={questionitem.id}
                                value="don't know"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "idontknow",
                                      defaultMessage: "Don't know",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == 
                                  intl.formatMessage({
                                    id: "idontknow",
                                    defaultMessage: "Don't know",
                                  }) &&
                                  questionitem.user_id == item.user
                                    ? "checked"
                                    : ""
                                }
                                required
                              />
                              <i className="fas fa-question"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div
                        className={
                          item.heading == intl.formatMessage({id: "renume", defaultMessage: "Remuneration"})
                           &&
                          this.state.remunerationstatus == true
                            ? "row"
                            : "row defaultnone" &&
                              item.heading == intl.formatMessage({id: "tutor", defaultMessage: "Tutoring"})
                               &&
                              this.state.tutoringsagree == true
                            ? "row"
                            : "row defaultnone"
                        }
                      >
                        <div className="col-md-7">
                          <p className="survey-question">
                            {questionitem.question}
                          </p>
                          {this.state.seen &&
                          questionitem.question ==
                          intl.formatMessage({id: "douensure", defaultMessage: "Do you ensure a reasonable ratio of interns per tutor?",
                          }) &&
                        <Popup toggle={() => this.setState({seen: false})}/>} 
                        </div>
                        <div className="col-md-5" key={i}>
                          <div className="answer-inputs">
                            <div className="ans-status">
                              <p>
                                {this.state.selected.find(
                                  (a) => a["id"] === questionitem.id
                                )
                                  ? this.state.selected.find(
                                      (a) => a["id"] === questionitem.id
                                    )["ans"]
                                  : questionitem.user_id == item.user &&
                                    questionitem.answer}
                              </p>
                            </div>
                            <div className="ans-selection like">
                              <input
                                title={intl.formatMessage({
                                  id: "agree",
                                  defaultMessage: "Agree",
                                })}
                                type="radio"
                                id="rec-q-1-like"
                                name={questionitem.id}
                                value="agree"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "agree",
                                      defaultMessage: "Agree",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == intl.formatMessage({
                                    id: "agree",
                                    defaultMessage: "Agree",
                                  })
                                    ? "checked"
                                    : ""
                                }
                                required={
                                  item.heading == intl.formatMessage({id: "renume", defaultMessage: "Remuneration"})
                                   &&
                                  this.state.remunerationstatus == true
                                    ? "required"
                                    : "" &&
                                      item.heading == intl.formatMessage({id: "tutor", defaultMessage: "Tutoring"})
                                       &&
                                      this.state.tutoringsagree == true
                                    ? "required"
                                    : ""
                                }
                              />
                              <i className="far fa-thumbs-up"></i>
                            </div>
                            <div className="ans-selection dislike">
                              <input
                                title={intl.formatMessage({
                                  id: "disagree",
                                  defaultMessage: "Disagree",
                                })}
                                type="radio"
                                id="rec-q-1-dislike"
                                name={questionitem.id}
                                value="disagree"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "disagree",
                                      defaultMessage: "Disagree",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == intl.formatMessage({
                                    id: "disagree",
                                    defaultMessage: "Disagree",
                                  })
                                    ? "checked"
                                    : ""
                                }
                              />
                              <i className="far fa-thumbs-down"></i>
                            </div>
                            <div className="ans-selection dnknow">
                              <input
                                title={intl.formatMessage({
                                  id: "idontknow",
                                  defaultMessage: "Don't know",
                                })}
                                type="radio"
                                id="other"
                                name={questionitem.id}
                                value="don't know"
                                onClick={() => {
                                  this.legalbox(
                                    questionitem.id,
                                    intl.formatMessage({
                                      id: "idontknow",
                                      defaultMessage: "Don't know",
                                    }),
                                    questionitem.question,
                                    questionitem.heading_id,
                                    item.heading
                                  );
                                }}
                                defaultChecked={
                                  questionitem.answer == 
                                    intl.formatMessage({
                                    id: "idontknow",
                                    defaultMessage: "Don't know",
                                  })
                                    ? "checked"
                                    : ""
                                }
                              />
                              <i className="fas fa-question"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  )}
                </React.Fragment>
              ))}
              <div className="row">
                <div className="offset-md-2  col-md-4">
                  <input
                    type="button"
                    class="pre-nxt-btn"
                    id="next-button"
                    value={intl.formatMessage({
                      id: "gotoprev",
                      defaultMessage: "Go to previous step",
                    })}
                    onClick={this.gotoPrvStp.bind(this)}
                  />
                </div>
                <div className="col-md-4">
                  <input
                    onClick={this.handleYesStep2}
                    type="submit"
                    class="pre-nxt-btn"
                    id="next-button"
                    value={intl.formatMessage({
                      id: "gotonext",
                      defaultMessage: "Go to next step",
                    })}
                  />
                </div>
              </div>
            </Form>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default injectIntl(withRouter(Step2));
